﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Crud_backend_incca.Migrations
{
    public partial class UltimoMigrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Funcionario",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Funcionario_Nome",
                table: "Funcionario",
                column: "Nome",
                unique: true,
                filter: "[Nome] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Funcionario_Nome",
                table: "Funcionario");

            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Funcionario",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
