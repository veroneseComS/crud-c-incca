﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crud_backend_incca.Models;
using Microsoft.EntityFrameworkCore;
using System.Web.Http.Cors;

namespace Crud_backend_incca.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmpresaController : ControllerBase
    {
        private readonly CrudContext _context;

        public EmpresaController(CrudContext context)
        {
            _context = context;
        }

        //GET: api/empresa
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empresa>>> getEmpresas()
        {
            var empresas = await _context.Empresa.Include(t => t.Funcionarios).ToListAsync();
            return Ok(empresas);
        }

        //GET: api/empresa/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Empresa>> getEmpresa(int id)
        {
            var Empresa = await _context.Empresa.FindAsync(id);

            if(Empresa == null)
            {
                return NotFound();
            }

            return Empresa;
        }

        //POST api/empresa
        [HttpPost]
        public async Task<ActionResult<Empresa>> cadastraEmpresa(Empresa empresa)
        {
            _context.Empresa.Add(empresa);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(getEmpresa), new { id = empresa.EmpresaId }, empresa);
        }

        //PUT api/empresa
        [HttpPut("{id}")]
        public async Task<IActionResult> AlteraEmpresa(int id, Empresa empresa)
        {
            if(id != empresa.EmpresaId)
            {
                return BadRequest();
            }

            _context.Entry(empresa).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        //DELETE api/empresa
        [HttpDelete("{id}")]
        public async Task<IActionResult> deletaEmpresa(int id)
        {
            var empresa = await _context.Empresa.FindAsync(id);

            if(empresa == null)
            {
                return NotFound();
            }

            _context.Empresa.Remove(empresa);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }

}
