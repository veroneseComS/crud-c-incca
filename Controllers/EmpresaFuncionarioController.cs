﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crud_backend_incca.Models;
using Microsoft.EntityFrameworkCore;

namespace Crud_backend_incca.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaFuncionarioController : ControllerBase
    {

        private readonly CrudContext _context;

        public EmpresaFuncionarioController(CrudContext context)
        {
            _context = context;
        }

        // GET api/empresafuncionario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empresa>>> getNome()
        {
            return await _context.Empresa.Include(t => t.Funcionarios).ToListAsync();
        }

    }
}
