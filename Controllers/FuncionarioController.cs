﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Crud_backend_incca.Models;
using Microsoft.EntityFrameworkCore;


namespace Crud_backend_incca.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionarioController : ControllerBase
    {

        private readonly CrudContext _context;

        public FuncionarioController(CrudContext context)
        {
            _context = context;
        }

        // GET api/funcionario
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Funcionario>>> getFuncionarios()
        {
            return await _context.Funcionario.Include("Empresas").ToListAsync();
        }

        // GET api/funcionario/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Funcionario>> getFuncionario(int id)
        {
            var Funcionario = await _context.Funcionario.Include("Empresas").SingleOrDefaultAsync(i => i.FuncionarioId == id);

            if (Funcionario == null)
            {
                return NotFound();
            }

            return Funcionario;
        }

        // POST api/funcionario
        [HttpPost]
        public async Task<ActionResult<Funcionario>> cadastraFuncionario(Funcionario funcionario)
        {
            _context.Funcionario.Add(funcionario);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(getFuncionario), new { id = funcionario.FuncionarioId }, funcionario);
        }

        // PUT api/funcionario/5
        [HttpPut("{id}")]
        public async Task<IActionResult> AlteraFuncionario(int id, Funcionario funcionario)
        {
            if(id != funcionario.FuncionarioId)
            {
                return BadRequest();
            }

            _context.Entry(funcionario).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE api/empresa/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> deletaFuncionario(int id)
        {
            var funcionario = await _context.Funcionario.FindAsync(id);

            if(funcionario == null)
            {
                return NotFound();
            }

            _context.Funcionario.Remove(funcionario);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
