﻿using Microsoft.EntityFrameworkCore;

namespace Crud_backend_incca.Models
{
    public class CrudContext : DbContext
    {
        public CrudContext(DbContextOptions<CrudContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Funcionario> Funcionario { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Funcionario>()
                .HasIndex(f => f.Nome)
                .IsUnique();
        }

    }
}
