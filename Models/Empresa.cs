﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Crud_backend_incca.Models
{
    public class Empresa
    {
        public int EmpresaId { get; set;}
        public string Nome { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public Nullable<int> Numero { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string UF { get; set; }
        public Nullable<int> Unidade { get; set; }
        public Nullable<int> IBGE { get; set; }
        public Nullable<int> GIA { get; set; }
        public string Telefone{get;set;}
        public ICollection<Funcionario> Funcionarios { get; set; }
    }
}
