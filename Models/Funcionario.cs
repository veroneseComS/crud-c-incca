﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;


namespace Crud_backend_incca.Models
{
    public class Funcionario
    {
        public int FuncionarioId { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public Nullable<float> Salario { get; set; }
        //Foreign Key
        public int EmpresaId { get; set; }
        public Empresa Empresas { get; set; }
    }
}
