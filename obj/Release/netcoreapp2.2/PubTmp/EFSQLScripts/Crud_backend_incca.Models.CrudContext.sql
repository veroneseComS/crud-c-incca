﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190613005057_1')
BEGIN
    CREATE TABLE [Empresa] (
        [EmpresaId] int NOT NULL IDENTITY,
        [Nome] nvarchar(max) NULL,
        [Cep] nvarchar(max) NULL,
        [Logradouro] nvarchar(max) NULL,
        [Complemento] nvarchar(max) NULL,
        [Numero] int NULL,
        [Bairro] nvarchar(max) NULL,
        [Localidade] nvarchar(max) NULL,
        [UF] nvarchar(max) NULL,
        [Unidade] int NULL,
        [IBGE] int NULL,
        [GIA] int NULL,
        [Telefone] nvarchar(max) NULL,
        CONSTRAINT [PK_Empresa] PRIMARY KEY ([EmpresaId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190613005057_1')
BEGIN
    CREATE TABLE [Funcionario] (
        [FuncionarioId] int NOT NULL IDENTITY,
        [Nome] nvarchar(max) NULL,
        [Cargo] nvarchar(max) NULL,
        [Salario] real NULL,
        [EmpresaId] int NOT NULL,
        CONSTRAINT [PK_Funcionario] PRIMARY KEY ([FuncionarioId]),
        CONSTRAINT [FK_Funcionario_Empresa_EmpresaId] FOREIGN KEY ([EmpresaId]) REFERENCES [Empresa] ([EmpresaId]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190613005057_1')
BEGIN
    CREATE INDEX [IX_Funcionario_EmpresaId] ON [Funcionario] ([EmpresaId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190613005057_1')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190613005057_1', N'2.2.4-servicing-10062');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190614002804_UltimoMigrate')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Funcionario]') AND [c].[name] = N'Nome');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Funcionario] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [Funcionario] ALTER COLUMN [Nome] nvarchar(450) NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190614002804_UltimoMigrate')
BEGIN
    CREATE UNIQUE INDEX [IX_Funcionario_Nome] ON [Funcionario] ([Nome]) WHERE [Nome] IS NOT NULL;
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190614002804_UltimoMigrate')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190614002804_UltimoMigrate', N'2.2.4-servicing-10062');
END;

GO

